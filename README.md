# Introduction
The base Arrowhead Framework consists of three core services: Service Registry, Authorization, and Orchestrator. All are written in Java with Spring Framework. To run the core services the following are required:
* Java JRE version 11
* MySQL version 5.7+ or MariaDB version 10.2+

By default, the following ports are used by particular services:
* MySQL/MariaDB: 3306
* Service Registry: 8443
* Authorization: 8445
* Orchestrator: 8441

For now, all the services are indented to run in non-secure mode to avoid using certificates and HTTPS in the first tests. In the final implementation, the secured mode should be used, and the installation script will be updated accordingly, and all the database passwords will be generated automatically.

# Installation

The installation might be either semiautomatic or manual, but in both cases the commands should be executed on the field gateway (e.g. through ssh). The first approach uses an installation script (attached) install.sh. To minimize the space used by the installation, the headless version of the JRE is used.

As a result of both installation processes, the following will be downloaded, installed or created:
* MariaDB server and OpenJDK JRE ver. 11 headless
* Arrowhead core services in /usr/local/ArrowheadFramework/
* Initiated Arrowhead database
* Arrowhead core services configured as systemctl services


The following procedures were tested on Raspberry Pi 4 with Debian Buster based OS and ARMv7 architecture.

## OPTION 1: Semiautomatic installation

To make the installation (semi)automatic, a shell script `install.sh` has been written. 

To execute the script, the script should be placed somewhere on the target device and the following command should be executed as super-user:

```sh
sudo bash install.sh
```

The script prompts user to set the root password for the MariaDB database, then proceeds to download the Arrowhead core services from google drive (to avoid backward compatibility issues when downloading from the Arrowhead github repository; the downloading from google drive takes some time). 

There is a possibility to change the Arrowhead root folder by changing the line in the `install.sh` script: 

```sh
export arrowheadRootDir=/usr/local/ArrowheadFramework
```

To change the ports at which the particular services are available, the appropriate `application.properties` file in `arorwheadRootDirectory/service_name` should be changed in line `server.port=XXXX`, where XXXX is the desired port number.

## OPTION 2: Manual installation

The manual installation yields the same results as the semiautomatic one, whereas a more control over the execution of particular steps can be maintained. 

First, the Java Runtime Environment should be installed along with the MariaDB (since MySQL is unavailable for ARMv7) using the following commands:

```sh
sudo apt update
sudo apt install openjdk-11-jre-headless
sudo apt install mariadb-server
```

After successfully installing the prerequisites, all the files except this instruction, installation script and folder “definitions-of-services” should be downloaded from `https://gitlab.com/bettersolutions/open-source/ah-arm-insecure-deployment` and then moved to the desired folder on the gateway (e.g. `/usr/local/ArrowheadFramework/`).

The content of folder “definition-of-services” should be moved to `/etc/systemd/system/`. If the root folder of Arrowhead differs from `/usr/local/ArrowheadFramework/`, the *.service files should be changed accordingly.

Appropriate execution right should be given to the service files through the following commands:
```sh
sudo chmod 664 /etc/systemd/system/ahserviceregistry.service
sudo chmod 664 /etc/systemd/system/ahauthorization.service
sudo chmod 664 /etc/systemd/system/ahorchestrator.service
```

Finally, the services should be enabled. In order to do so, the following commands should be executed:

```sh
sudo systemctl daemon-reload
sudo systemctl enable ahserviceregistry.service
sudo systemctl enable ahauthorization.service
sudo systemctl enable ahorchestrator.service
sudo systemctl daemon-reload
```

# Verification of the installation

After the boot of the device, after some time has passed the Arrowhead core services should be up and running. Note that it takes longer for Orchestator and Authorization to load than for Service Registry (on Raspberry Pi 4 it takes almost 2 minutes to start everything). To check if the services are up, the following methods can be used:

 
1. Access the Swagger-UI of a specific service through the web browser in the same local network, by entering the IP address of the device with the appropriate port, for example:

```
192.168.1.222:8443
```

after accessing such an address, you should see the Swagger-UI homepage of Service Registry.

2. Send an “echo” request to a specific service using CURL, for example:

```sh
curl -X GET "http://192.168.1.222:8445/authorization/echo"
```

where port and “authorization” is replaced by particular port and service name (serviceregistry or orchestrator). The service should respond with acknowledgement: “Got it!”.

3. Check status with systemctl tool using the following command:

```sh
sudo systemctl ahserviceregistry status
```

If everything is ok, the status should be shown as “Active: active (running)” with the application log below. For authorization use “ahauthorization”, and for orchestrator use “ahorchestrator”.

4. Troubleshooting

Sometimes, wget is unable to properly download the large ‘.jar’ files, therefore the services will fail to run. In this case, if the size of the ‘.jar’ files is lower than 50 Mb, the script should be executed once again (or the jar files should be downloaded manually).