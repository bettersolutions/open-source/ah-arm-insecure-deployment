#!/bin/bash

# Prepare the directiories
export arrowheadRootDir=/usr/local/ArrowheadFramework
mkdir $arrowheadRootDir
mkdir $arrowheadRootDir/serviceregistry
mkdir $arrowheadRootDir/authorization
mkdir $arrowheadRootDir/orchestrator
cd $arrowheadRootDir

# Download and configure wget, Java and MariaDB

read -sp 'Set your Arrowhead database password: ' ahrootpass
apt -y update

debconf-set-selections <<< 'mariadb-server mysql-server/root_password password $ahrootpass'
debconf-set-selections <<< 'mariadb-server mysql-server/root_password_again password $ahrootpass'
apt -y install mariadb-server 
apt -y install wget
apt -y install openjdk-11-jre-headless


# Download Arrowhead Framework core services

cd $arrowheadRootDir/serviceregistry

wget -O serviceregistry.jar https://gitlab.com/bettersolutions/open-source/ah-arm-insecure-deployment/-/raw/master/serviceregistry/serviceregistry.jar?inline=false
wget -O application.properties https://gitlab.com/bettersolutions/open-source/ah-arm-insecure-deployment/-/raw/master/serviceregistry/application.properties?inline=false
wget -O log4j2.xml https://gitlab.com/bettersolutions/open-source/ah-arm-insecure-deployment/-/raw/master/serviceregistry/log4j2.xml?inline=false

cd $arrowheadRootDir/authorization

wget -O authorization.jar https://gitlab.com/bettersolutions/open-source/ah-arm-insecure-deployment/-/raw/master/authorization/authorization.jar?inline=false
wget -O application.properties https://gitlab.com/bettersolutions/open-source/ah-arm-insecure-deployment/-/raw/master/authorization/application.properties?inline=false
wget -O log4j2.xml https://gitlab.com/bettersolutions/open-source/ah-arm-insecure-deployment/-/raw/master/authorization/log4j2.xml?inline=false

cd $arrowheadRootDir/orchestrator

wget -O orchestrator.jar https://gitlab.com/bettersolutions/open-source/ah-arm-insecure-deployment/-/raw/master/orchestrator/orchestrator.jar?inline=false
wget -O application.properties https://gitlab.com/bettersolutions/open-source/ah-arm-insecure-deployment/-/raw/master/orchestrator/application.properties?inline=false
wget -O log4j2.xml https://gitlab.com/bettersolutions/open-source/ah-arm-insecure-deployment/-/raw/master/orchestrator/log4j2.xml?inline=false

# Download and execute database initializer
cd $arrowheadRootDir
wget -c --no-check-certificate https://gitlab.com/bettersolutions/open-source/ah-arm-insecure-deployment/-/raw/master/initdb.sql?inline=false -O initdb.sql
mysql -uroot -p$ahrootpass < initdb.sql

# Prepare AH services as systemd services

## Service Registry

touch  /etc/systemd/system/ahserviceregistry.service
echo "[Unit]
Description=Arrowhead Framework Service Registry
After=network.service mysql.service

[Service]
WorkingDirectory=$arrowheadRootDir/serviceregistry/
Type=simple
Restart=always
ExecStart=/usr/bin/java -Dlog4j.configurationFile=file:$arrowheadRootDir/serviceregistry/log4j2.xml -jar $arrowheadRootDir/serviceregistry/serviceregistry.jar
ExecStartPost=/bin/bash -c 'sleep 10'
TimeoutStopSec=5

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/ahserviceregistry.service
chmod 664 /etc/systemd/system/ahserviceregistry.service

## Authorization

touch  /etc/systemd/system/ahauthorization.service
echo "[Unit]
Description=Arrowhead Framework Authorization Service
After=network.service mysql.service ahserviceregistry.service

[Service]
WorkingDirectory=$arrowheadRootDir/authorization/
Type=simple
Restart=always
ExecStart=/usr/bin/java -Dlog4j.configurationFile=file:$arrowheadRootDir/authorization/log4j2.xml  -jar $arrowheadRootDir/authorization/authorization.jar

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/ahauthorization.service
chmod 664 /etc/systemd/system/ahauthorization.service


## Orchestrator

touch  /etc/systemd/system/ahorchestrator.service
echo "[Unit]
Description=Arrowhead Framework Orchestrator Service
After=network.service mysql.service ahserviceregistry.service

[Service]
WorkingDirectory=$arrowheadRootDir/orchestrator/
Type=simple
Restart=always
ExecStart=/usr/bin/java -Dlog4j.configurationFile=file:$arrowheadRootDir/orchestrator/log4j2.xml  -jar $arrowheadRootDir/orchestrator/orchestrator.jar

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/ahorchestrator.service
chmod 664 /etc/systemd/system/ahorchestrator.service

systemctl daemon-reload
systemctl enable ahserviceregistry.service
systemctl enable ahauthorization.service
systemctl enable ahorchestrator.service
systemctl daemon-reload
systemctl start ahserviceregistry.service
systemctl start ahauthorization.service
systemctl start ahorchestrator.service

